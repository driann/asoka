<!DOCTYPE html>
<html lang="en">
<?php include('inc/sig.php');?>
  <head>
    <title>:: Asoka Homepage ::</title>
    <?php include('inc/load_top.php');?>
  </head>
  <body>

    <!-- Wrap all page content here -->
    <div id="wrap">

    <?php include('inc/header.php');?>
    
    <div id="scroller-wrapper">  
     <div class="entry-meta">&nbsp;</div>
       <div class="entry-content">
        <div id="scrollerContainer3">
                                   
        <ul class="Content" style="display:none;">
         <?php for($i=1; $i<5; $i++){ ?>
          <li>
           <img class="Image" src="uploads/asoka-banner-left.png" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit." title="Lorem ipsum dolor sit amet, consectetur adipiscing elit." />
           <img class="Thumb" src="uploads/asoka-banner-left.png" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit." title="Lorem ipsum dolor sit amet, consectetur adipiscing elit." />
           <span class="Title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
           <span class="Caption"></span>
           <span class="Media"></span>
           <span class="LightboxMedia"></span>
           <span class="Link"></span>
           <span class="Target">_blank</span>
          </li>
          <li>
           <img class="Image" src="uploads/asoka-banner-right.png" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit." title="Lorem ipsum dolor sit amet, consectetur adipiscing elit." />
           <img class="Thumb" src="uploads/asoka-banner-right.png" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit." title="Lorem ipsum dolor sit amet, consectetur adipiscing elit." />
           <span class="Title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
           <span class="Caption"></span>
           <span class="Media"></span>
           <span class="LightboxMedia"></span>
           <span class="Link"></span>
           <span class="Target">_blank</span>
          </li>
         <?php } ?>
        </ul>                                   
      </div>
    </div>
      
    <div class="content-bg">     
     <div class="container">
      <div class="row page">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 left-column">

      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 center-column">
      <h2>ASOKA ISSUE FIFTEEN: KLAPFSEE, AUSTRIA.</h2>
      <h5>Words by James Cartwright Photographs by Pelle Crepin Styling by Aradia Crockett</h5>

      <p>Hans Ulrich Obrist, critic, art historian and curator at the Serpentine Galleries in London, tells us how he assembles the objects next to his bed. Lorem ipsum dolor sit amet, quem scaevola ex nec, te ludus malorum his. Ea nam eirmod consequuntur. Quando eruditi ad vix, no ius dicam nonumes. In dico commodo omittantur mei, sea inermis albucius instructior ad, ridens nostro eos ne.</p>
      <p>Habeo prompta evertitur an his. Vis nostro diceret lucilius an, in pro dicat interesset. Mea brute soleat ex, sit error nobis posidonium ut, denique detraxit et vim. Id tation tibique perfecto vim, cu eum apeirian tractatos laboramus. Ad est nulla dolor tantas.</p>
      <p>Everti scripta moderatius eam eu. Usu no timeam gloriatur percipitur. Et posse latine vituperata pri, eum an harum graeci consequat. Usu ad vide ferri neglegentur, enim imperdiet te eam. Est decore laudem ne.</p>
      <p>Duo an labore volumus, at quo assum fabulas antiopam. Mea quas reque menandri et, ne has dignissim sadipscing, an mel veniam comprehensam. Ea est graecis delectus patrioque, no has porro efficiantur. Nam et tollit nostro aperiri, dicunt constituto duo et.</p>
      <p>Sint singulis repudiandae nec ea, aperiam concludaturque cum et, solum vocent omnesque ei sea. Vis an modo porro. Nec no deseruisse dissentiunt. Vis id movet prompta, ut vim omnes philosophia definitionem. </p>
      <p>Et porro blandit principes nec. Tritani moderatius posidonium no sit, at cibo quaeque ponderum est, his eu ignota quodsi dolores.</p>
      <p>Lorem ipsum dolor sit amet, quem scaevola ex nec, te ludus malorum his. Ea nam eirmod consequuntur. Quando eruditi ad vix, no ius dicam nonumes. In dico commodo omittantur mei, sea inermis albucius instructior ad, ridens nostro eos ne. Diam porro pri at.</p>
      <p>Habeo prompta evertitur an his. Vis nostro diceret lucilius an, in pro dicat interesset. Mea brute soleat ex, sit error nobis posidonium ut, denique detraxit et vim. Id tation tibique perfecto vim, cu eum apeirian tractatos laboramus. Ad est nulla dolor tantas.</p>
      <p>Everti scripta moderatius eam eu. Usu no timeam gloriatur percipitur. Et posse latine vituperata pri, eum an harum graeci consequat. Usu ad vide ferri neglegentur, enim imperdiet te eam. Est decore laudem ne.</p>
      <p>Duo an labore volumus, at quo assum fabulas antiopam. Mea quas reque menandri et, ne has dignissim sadipscing, an mel veniam comprehensam. Ea est graecis delectus patrioque, no has porro efficiantur. Nam et tollit nostro aperiri, dicunt constituto duo et.</p>
      <p>Sint singulis repudiandae nec ea, aperiam concludaturque cum et, solum vocent omnesque ei sea. Vis an modo porro. Nec no deseruisse dissentiunt. Vis id movet prompta, ut vim omnes philosophia definitionem. Et porro blandit principes nec. Tritani moderatius posidonium no sit, at cibo quaeque ponderum est, his eu ignota quodsi dolores.</p>

      <span><a href="#">Read More</a></span>
      
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 right-column">
      </div>
      </div> 
     </div>
    </div> <!-- .content-bg -->


    </div><!-- #wrap -->

    <?php include('inc/footer.php');?>

    <?php include('inc/load_bottom.php');?>

  </body>
</html>