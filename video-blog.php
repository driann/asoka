<!DOCTYPE html>
<html lang="en">
<?php include('inc/sig.php');?>
  <head>
    <title>:: Asoka Homepage ::</title>
    <?php include('inc/load_top.php');?>
  </head>
  <body>

    <!-- Wrap all page content here -->
    <div id="wrap">

    <?php include('inc/header.php');?>
      
      
    <div class="content-bg">
   
    <div class="container">
     <div id="cat-heading">
       <span> Video Blog </span>
     </div>
    </div>
    
    <div class="container">
      <div id="content" class="video-blog">
        <ul class="video-list">
         <?php for($i=1; $i<9; $i++){ ?>
          <li class="video-item">
           <a href="#" class="thumb-article"><img class="img-responsive thumb" src="uploads/video.png" alt="Generic placeholder image" >
           <span class="play-button"><img class="img-responsive" src="uploads/play.png" alt="Generic placeholder image" ></span></a>
           <div class="caption">
            <p>Donec sed odio dui.</p>
           </div>
          </li>
         <?php } ?>
         </ul>
      </div><!-- #content -->
     </div><!-- .container -->
     </div> <!-- .content-bg -->


    </div><!-- #wrap -->

    <?php include('inc/footer.php');?>

    <?php include('inc/load_bottom.php');?>

  </body>
</html>